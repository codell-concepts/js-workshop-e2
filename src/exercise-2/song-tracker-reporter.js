import chalk from 'chalk';

export const toConsole = (played) => {
  for (const key in played) {
    console.log(chalk.yellow.bold(`Song ${key} has been played ${played[key]} time(s)`));
  }
};

export default toConsole;
