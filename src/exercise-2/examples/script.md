> Transpiling 

1. copy index.js into exercise-2
   * change *const chalk = require('chalk');* to import chalk from ('chalk'); 
   * add start:e2 script to package.json
   * *npm run start:e2* and....**boom!**
2. *npm i --save-dev babel-cli babel-preset-es2015 babel-preset-stage-0*
3. configure babel runtime (*fsutil file createnew .babelrc 0*)
4. build script package.json
    * *"build": "babel ./src -d ./dist"*
    * *npm run build*
    * view dist folder

> Workflow

1. npm i --save-dev nodemon
2. build:watch script
    * *npm run build -- --watch*

> ES6

1. modules
   * export
       * default
   * import
       * default
       * \* as ...
       * destructuring
   * Why modules be good
2. arrows
3. string templates
4. var vs let vs const
5. classes
    * constructors
    * default parameters
    * encapsulation
    * this
    * readonly
6. enhanced literals
7. Spread operator
