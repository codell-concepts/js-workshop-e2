import mouth from './mouth';
import Eye from './Eye';

// export function eat() {
//   mouth.chew(food.banana);
// }

export const eat = (food) => {
  mouth.chew(food);
};

export function drink(beverage) {
  mouth.drink(beverage);
}

export function sleep() {
  const left = new Eye('left', 'blue');
  const right = new Eye('right');
  left.close();
  right.close();
}

