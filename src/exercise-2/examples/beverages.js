export const soylant = { name: 'soylant' };
export const milk = { name: 'milk' };
export const water = { name: 'water' };

export default [
  soylant,
  milk,
  water
];
