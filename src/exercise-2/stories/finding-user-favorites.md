## Finding User Favorites

*As a user*  
*I would like to be able to view my favorite songs*  
*so that I can listen to my favorite beats.*

*Assumptions:*

* All genres are in the song list
* All artists are in the song list
* User can have only one preferred genre
* User can have only one preferred artist

### Acceptance Criteria

**Scenario #1 - User has a preferred genre**

>Given a list of songs  
And a user with a preferred genre  
And the user does not have a preferred artist  
When asking for favorite songs  
Then should only find songs related to user's preferred genre

**Scenario #2 - User has a preferred artist**

>Given a list of songs  
And a user with a preferred artist  
And the user does not have a preferred genre  
When asking for favorite songs  
Then should only find user's preferred artist songs

**Scenario #3 - User has a preferred genre and artist**

>Given a list of songs  
And a user with a preferred genre  
And the user has a preferred artist  
When asking for favorite songs  
Then should find user's preferred genre and artist songs

**Scenario #4 - User does not have any preferences**

>Given a list of songs  
And a user does not have a preferred artist  
And the user does not have a preferred genre  
When asking for favorite songs  
Then should not find any songs