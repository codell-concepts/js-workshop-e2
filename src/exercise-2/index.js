import chalk from 'chalk';

import User from './user';
import songs from './song-list';
import { play } from './song-player';
import tracker from './song-tracker';
import { toConsole } from './song-tracker-reporter';

console.log(chalk.red('-- Begin --'));
const preferences = { genre: 'alternative', artist: 'Snoop Dogg' };
const user = new User(preferences, songs);

console.log(chalk.blue('Loading your favorites...'));
const favorites = user.getFavoriteSongs();
console.log(chalk.blue.bold(`Found ${favorites.length} favorites`));

console.log(chalk.cyan('Playing your first favorite...'));
const firstSong = favorites[0];
play(firstSong);
console.log(chalk.cyan.bold(`Now playing '${firstSong.title}' by '${firstSong.artist}'`));

console.log(chalk.yellow('Running song tracking report...'));
tracker.generateReport(toConsole);

console.log(chalk.red('-- End --'));
