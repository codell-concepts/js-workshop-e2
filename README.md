## Boardwalk JavaScript Workshop - Exercise 2

> #### Goals

* Development workflow & Nodemon
* ES6
* Transpiling - Babel

> #### Developer Setup

To clone and run this repository you'll need [Git](https://git-scm.com) and [Node.js](https://nodejs.org/en/download/) (which comes with [npm](http://npmjs.com)) installed on your computer. From your command line:

``` bash
# Go to your projects directory
cd wherever-you-store-your-projects

# Clone this repository
git clone https://codell-concepts@bitbucket.org/codell-concepts/js-workshop-e2.git

# Go into the repository
cd js-workshop-e2

# install dependencies
npm install

```

> #### Running exercises

``` bash
# start exercise 1 (ecosystem)
npm run start:e1

# start exercise 2 (es6)
npm run start:e2

```

> #### Building

``` bash

# build to dist
npm run build

# Clean dist
npm run clean

```